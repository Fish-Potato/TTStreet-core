package com.tts.component.datasource;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Service;

/**
 * Created by zhaoqi on 2016/9/14 0014.
 */
@Aspect
public class DataSourceAspectJ {

    @Pointcut("@annotation(com.tts.component.datasource.Master)")
    public void masterPointCut(){

    }

    @Pointcut("@annotation(com.tts.component.datasource.Slave)")
    public void slavePointCut(){

    }

    @Around("masterPointCut()")
    public void forceMaster(ProceedingJoinPoint joinPoint) throws Throwable{
        DataSourceHandler.setDataSources("master");
        joinPoint.proceed();
    }

    @Around("slavePointCut()")
    public void forceSlave(ProceedingJoinPoint joinPoint) throws Throwable{
        DataSourceHandler.setDataSources("slave");
        joinPoint.proceed();
    }
}
