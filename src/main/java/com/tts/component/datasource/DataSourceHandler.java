package com.tts.component.datasource;

/**
 * Created by zhaoqi on 2016/9/14 0014.
 */
public class DataSourceHandler {

    private static ThreadLocal<String> dataSources = new ThreadLocal<>();

    public static String getDataSource() {
        return dataSources.get();
    }

    public static void setDataSources(String dataSource) {
        dataSources.set(dataSource);
    }
}
