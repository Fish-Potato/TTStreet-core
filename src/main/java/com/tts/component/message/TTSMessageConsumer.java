package com.tts.component.message;

/**
 * Created by zhaoqi on 2016/9/1 0001.
 */
public interface TTSMessageConsumer {
    String getMessageName();
    void handleMessage(Object message);
}
