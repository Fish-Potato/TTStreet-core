package com.tts.component.message;

import org.apache.activemq.command.ActiveMQQueue;
import org.apache.camel.component.jms.SimpleJmsMessageListenerContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.jms.listener.SimpleMessageListenerContainer;
import org.springframework.jms.listener.adapter.MessageListenerAdapter;
import org.springframework.jms.support.converter.SimpleMessageConverter;

import javax.jms.ConnectionFactory;
import java.util.Map;

/**
 * Created by zhaoqi on 2016/9/1 0001.
 */
public class MessageConsumerFactory implements ApplicationContextAware{

    private final Logger logger = LoggerFactory.getLogger(MessageConsumerFactory.class);

    private Map<String,TTSMessageConsumer> messageConsumerMap;

    private ConnectionFactory connectionFactory;

    private SimpleMessageConverter simpleMessageConverter;

    private int concurrentConsumer;

    private String serviceName;

    public MessageConsumerFactory(ConnectionFactory connectionFactory, SimpleMessageConverter simpleMessageConverter,int concurrentConsumer,String serviceName) {
        this.connectionFactory = connectionFactory;
        this.simpleMessageConverter = simpleMessageConverter;
        this.concurrentConsumer = concurrentConsumer;
        this.serviceName = serviceName;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        messageConsumerMap = applicationContext.getBeansOfType(TTSMessageConsumer.class);
        this.addListener();
    }

    private void addListener() {
        if (null == messageConsumerMap) {
            return ;
        }
        try {
            for (TTSMessageConsumer messageConsumer : messageConsumerMap.values()) {
                SimpleMessageListenerContainer messageListenerContainer = new SimpleMessageListenerContainer();
                messageListenerContainer.setConnectionFactory(connectionFactory);
                messageListenerContainer.setMessageConverter(simpleMessageConverter);
                messageListenerContainer.setConcurrentConsumers(concurrentConsumer);

                MessageListenerAdapter adapter = new MessageListenerAdapter(messageConsumer);
                adapter.setMessageConverter(simpleMessageConverter);
                messageListenerContainer.setMessageListener(adapter);
                messageListenerContainer.setDestinationName(registerDestinationName(messageConsumer));

                messageListenerContainer.start();

                logger.info("starting amq container {} success, concurrentConsumers {}.", messageConsumer, concurrentConsumer);
            }
        } catch (Exception e) {
            logger.error("starting amq container failed " ,e);
        }

    }

    private String registerDestinationName(TTSMessageConsumer messageConsumer) {

        String queueName = "tts:" +serviceName +":" + messageConsumer.getMessageName();

        return queueName;
    }

}
